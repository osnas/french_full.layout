# french_full.layout

French full characters keyboard for SailfishOS FingerTerm.

Put the file french_full.layout in the directory /home/nemo/.config/FingerTerm. Start the FingerTerm, press Menu button ( located top right corner) -> VKB layout... -> french_full -> Select.

It is based on french layout. As I noticed, punctuation like normal keyboard didn't work- if you have any other idea of how to make it work, please let me know- , so I added two extra lines after the last one, with all combinations of punctuation. When you press 'Shift' you have the capital letters.

I added also Euro character (€), 8th line, 1nd key from right. Finally I added a SpaceBar 3keys-size at 7th line. 

In 8th line there is an empty key -you can use this "extra" key for any other character/ 
functionality if you need.

The format of the file is pretty simple, and easily to modify at your needs.

If you have any corrections/ suggestions, please let me know